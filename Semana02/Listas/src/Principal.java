
import tsb.listas.Nodo;
import java.util.Random;
import tsb.listas.Lista;



public class Principal {

    private static final int MAX_ELEMENTS = 100000;
    
    public static void main(String[] args) {
        
        Persona a = new Persona(123, "Juan");
        Persona b = new Persona(234, "Pedro");
        Comparable c = a;

        if (a.compareTo(b) < 0) {
            System.out.println("El menor es:" + a);
        } else {
            System.out.println("El menor es:" + b);
        }
        
        Lista l = new Lista();
        Random random = new Random();
        for (int i = 0; i < MAX_ELEMENTS; i++) {
            int x = random.nextInt(1000);
            l.insertarOrdenado(x);
        }
        
        //l.insertar("Casa");
        
        System.out.println("Listado");
        l.mostrar();                
        
        int suma = 0;
        for (int i = 0; i < MAX_ELEMENTS; i++) {
            suma += (Integer)l.get(i);
        }
        System.out.println("La suma es: " + suma);
       
    }

}

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author gfbett
 */
public class MiFrame extends Frame {
    
    private final Button btnChau;
    private final Button btnHola;
    private final TextField txtNombre;
    
    public MiFrame() {
        btnHola = new Button("Hola");
        btnChau = new Button("Chau");
        this.setSize(800, 600);
        this.setTitle("Hola TSB");
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
        Panel pnl = new Panel();
        pnl.setLayout(new FlowLayout(FlowLayout.RIGHT));
        pnl.add(btnHola);
        pnl.add(btnChau);
        btnHola.setPreferredSize(new Dimension(200, 40));
        btnChau.setPreferredSize(new Dimension(200, 40));
        this.add(pnl, BorderLayout.SOUTH);
        txtNombre = new TextField();
        this.add(txtNombre);
        txtNombre.setBounds(10, 200, 200, 30);
        btnHola.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                showMessageDialog(MiFrame.this, "Hola " + txtNombre.getText());
            }
        });
        
        btnChau.addActionListener(e -> showMessageDialog(MiFrame.this, "Chau"));
        
    }


    class BtnChauListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            JOptionPane.showMessageDialog(MiFrame.this, "Chau " + txtNombre.getText());
        }
    }
}

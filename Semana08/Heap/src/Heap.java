/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class Heap {
    
    int [] v;
    int size;
    
    public Heap() {
        this(10);
    }
    
    public Heap(int initialSize) {
        v = new int[initialSize];
        size = 0;
    }
    
    
    public void add(int x) {
        growIfNeeded();
    }

    private void growIfNeeded() {
        if (v.length == size) {
            int newSize = (int)(size * 1.5);
            int[] aux = new int[newSize];
            System.arraycopy(v, 0, aux, 0, size);
            v = aux;
        }
    }
}

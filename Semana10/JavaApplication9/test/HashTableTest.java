
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class HashTableTest {
   
    private HashTable table;
    
    @Before
    public void setup(){
        table = new HashTable();
    }
    
    @Test
    public void testEmptySizeZero() {
        assertTrue(table.isEmpty());
    }
    
    @Test
    public void testPutAffectsSize() {
        table.put(1, 1);
        assertEquals(1, table.size());
    }
    
    @Test
    public void getInsertedItem() {
        table.put(1, 42);
        int value = table.get(1);
        assertEquals(42, value);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void getInsertedItemFail() {
        table.put(1, 42);
        table.get(3);
    }

    @Test
    public void testOverride() {
        table.put(1, 11);
        table.put(1, 42);
        assertEquals(1, table.size());
    }

    @Test
    public void testOverrideGet() {
        table.put(1, 11);
        table.put(1, 42);
        assertEquals(1, table.size());
        assertEquals(42, table.get(1));
    }
    
    @Test
    public void testCollision() {
        table.put(112, 1);
        table.put(212, 2);
        assertEquals(2, table.size());
    }

    @Test
    public void testCollisionGet() {
        table.put(112, 1);
        table.put(212, 2);
        assertEquals(2, table.size());
        assertEquals(1, table.get(112));
        assertEquals(2, table.get(212));
    }
    
}

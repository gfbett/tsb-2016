/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsb.listas;

/**
 *
 * @author gfbett
 */
public class Lista {
    private Nodo frente;
    private int size;
    
    public Lista() {
        frente = null;
        size = 0;
    }
    
    public void insertar(Comparable x){
        if (!isHomogeneus(x)) {
            throw new IllegalArgumentException();
        } 
        Nodo p = new Nodo(x);
        p.next = frente;
        frente = p;
        size ++;
    }
    
    public void insertarOrdenado(Comparable x) {
        if (!isHomogeneus(x)) {
            throw new IllegalArgumentException();
        }
        Nodo p = frente, q = null;
        while (p != null && p.info.compareTo(x) < 0) {
            q = p;
            p = p.next;
        }
        
        Nodo nuevo = new Nodo(x);
        nuevo.next = p;
        if (q != null) {
            q.next = nuevo;
        } else {
            frente = nuevo;
        }
        size ++;
    }

    public void mostrar() {
        Nodo p = frente;
        while(p!=null) {
            System.out.println(p.info);
            p = p.next;
        }        
    }
    
    public Object get(int pos) {
        if (pos < 0 || pos >= size) {
            throw new IllegalArgumentException();
        }
        Nodo p = frente;
        for(int i = 0; i< pos; i++) {
            p = p.next;
        }
        return p.info;
    }

    private boolean isHomogeneus(Object x) {
        if (frente != null && frente.info.getClass() != x.getClass()) {
            return false;
        }
        return true;
    }
}

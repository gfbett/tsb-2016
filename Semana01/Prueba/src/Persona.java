public class Persona {
    
    private int dni;
    private String nombre;
    
    public Persona(int dni, String nombre) {
        this.dni = dni;
        this.nombre = nombre;
    }
    
    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    @Override
    public String toString() {
        return "Persona: " + dni + " - " + nombre;
    }
    
    public void hortaliza() {
        System.out.println("Hortaliza");
    }
    
    

    
    
    
    
}

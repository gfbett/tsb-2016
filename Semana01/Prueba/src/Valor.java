/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class Valor extends Nodo{
    private int valor;

    public Valor(int valor) {
        this.valor = valor;
    }

    @Override
    public int valuar() {
        return valor;
    }
}


import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class ArbolBusquedaTest {
    
    private ArbolBusqueda arbol;
    
    @Before
    public void setup() {
        arbol = new ArbolBusqueda();
    }
    
    @Test
    public void testContains() {
        arbol.add(1);
        assertTrue(arbol.contains(1));
    }
    
    @Test
    public void testContains2() {
        arbol.add(1);
        assertFalse(arbol.contains(2));
    }

    @Test
    public void testContains3() {
        arbol.add(2);
        arbol.add(1);
        arbol.add(3);
        assertTrue(arbol.contains(2));
        assertTrue(arbol.contains(1));
        assertTrue(arbol.contains(3));
    }
    
    @Test
    public void testSize() {
        arbol.add(1);
        arbol.add(2);
        arbol.add(3);
        assertEquals(3, arbol.size());
    }
}

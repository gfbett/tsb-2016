/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gfbett
 */
public class ArbolBusqueda {
    
    private Nodo raiz;
    private int size;
    
    public ArbolBusqueda() {
        raiz = null;
        size = 0;
    }
    
    public boolean contains(int x) {
        Nodo p = raiz;
        while(p != null) {
            if (p.info == x) {
                return true;
            } else {
                if (x > p.info) {
                    p = p.der;
                } else {
                    p = p.izq;
                }
            }
        }
        return false;
    }
    
    public String mostrarElementos() {
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        recorrer(raiz, sb);
        sb.append(" ]");
        return sb.toString();
    }
    
    private void recorrer(Nodo n, StringBuilder sb) {
        if (n != null) {
            recorrer(n.izq, sb);
            sb.append(n.info).append(", ");
            recorrer(n.der, sb);
        }
    }
    
    public void add(int x) {
        Nodo p = raiz, q = null;
        while (p != null) {
            if (p.info == x) {
                break;
            } else {
                q = p;
                if (x > p.info) {
                    p = p.der;
                } else {
                    p = p.izq;
                }
            }
        }
        //No existía
        if (p == null) {
            Nodo nuevo = new Nodo(x);
            if (q != null) {
                if (x > q.info) {
                    q.der = nuevo;
                } else {
                    q.izq = nuevo;
                }
            } else {
                raiz = nuevo;
            }
            size++;
        }

        
    }   

    int size() {
        return size;
    }        
        
    
    private static class Nodo {
        Nodo izq;
        Nodo der;
        int info;

        private Nodo(int x) {
            this.info = x;
        }
    }  
}

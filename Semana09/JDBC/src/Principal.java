import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;
        
public class Principal {
    public static void main(String[] args) {

        listarPersonas();
    }

    private static void listarPersonas() {
        try {
            Scanner scanner = new Scanner(System.in);
        
            System.out.println("Ingrese dni a buscar: ");
            String dni = scanner.nextLine();
            
            Connection connection = abrirConexion();
            String sql = "SELECT * FROM Personas WHERE dni = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, dni);
            
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                System.out.println("Id: " + result.getInt(1));
                System.out.println("Nombre: " + result.getString(2));
                System.out.println("Dni: " + result.getInt(3));
            }
            result.close();
            statement.close();
            connection.close();
        }
        catch(ClassNotFoundException ex) {
            System.err.println("Error cargando driver");
        }
        catch(SQLException | IOException ex) {
            System.err.println("Error!!!");
        }
    }

    private static Connection abrirConexion() throws IOException, ClassNotFoundException, SQLException {
        Properties properties = new Properties();
        properties.load(Principal.class.getResourceAsStream("/dbconfig.properties"));
        Class.forName(properties.getProperty("driver"));
        Connection connection = DriverManager.getConnection(properties.getProperty("url"));
        connection.setAutoCommit(false);
        return connection;
    }
    
}

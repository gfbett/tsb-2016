
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author gfbett
 */
public class Principal {
    public static void main(String[] args) {
        System.out.println("inicio main");
        f1();
        System.out.println("fin main");
    }

    private static void f1() {
        System.out.println("inicio f1");
        f2();
        System.out.println("fin f1");
    }

    private static void f2() {
        System.out.println("inicio f2");
        try {
            f3();
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
        System.out.println("fin f2");
    }

    private static void f3() throws IOException{
        System.out.println("inicio f3");
        try {
            f4();
        } catch (Exception  ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        System.out.println("fin f3");
    }

    private static void f4() {
        System.out.println("inicio f4");
        try (RandomAccessFile rf  = 
                new RandomAccessFile("test.txt", "rw")){
            rf.writeChars("Hola mundo");
        } catch (IOException e) {
            System.err.println("Error!");
        }
        System.out.println("fin f4");
    }
}

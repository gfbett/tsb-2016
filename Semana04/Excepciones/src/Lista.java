/**
 *
 * @author gfbett
 */
public class Lista {
    
    private Nodo frente;
    private int size;

    public int size() {
        return size;
    }

    public void insertar(int i) {
        if (i < 0) throw new IllegalArgumentException("No acepta negativos");
        Nodo nuevo = new Nodo(i);
        nuevo.next = frente;
        frente = nuevo;
        size ++;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Nodo p = frente;
        while(p!=null) {
            sb.append(p.info);
            if (p.next != null) {
                sb.append(", ");
            }
            p = p.next;
        }
        sb.append("}");
        return sb.toString();
    }
    
    
    
    private static class Nodo {
        int info;
        Nodo next;
        
        public Nodo(int x) {
            info = x;
        }
    }
}
